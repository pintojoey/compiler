package LexicalAnalyzer;

public class Token {
public Token(String type, String value) {
		super();
		this.type = type;
		this.value = value;
	}
private String type;
private String value;
private boolean counted=false;
/**
 * @return the value
 */
public String getValue() {
	return value;
}
/**
 * @param value the value to set
 */
public void setValue(String value) {
	this.value = value;
}
/**
 * @return the type
 */
public String getType() {
	return type;
}
/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}
/**
 * @return the counted
 */
public boolean isCounted() {
	return counted;
}
/**
 * @param counted the counted to set
 */
public void setCounted(boolean counted) {
	this.counted = counted;
}
}
