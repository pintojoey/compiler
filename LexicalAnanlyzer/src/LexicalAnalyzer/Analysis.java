//package LexicalAnalyzer;
//
//import java.io.IOException;
//import java.io.StringReader;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.StringTokenizer;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import org.apache.commons.lang3.*;
//public class Analysis {
//	String data;
//	ArrayList<Token>tokenlist;
//	ArrayList<String>tokens;
//	String operator_regex="";
//	String keyword_regex="";
//	public Analysis(String data){
//		this.data=data;
//		tokens=new ArrayList<String>();
//		tokenlist=new ArrayList<Token>();
//		data=getStringConstants(data);
//
//		ArrayList<String> OperatorList = getOperatorList();
//		ArrayList<String> KeywordList=getKeywordsList();
//		Iterator<String> operatorIterator = OperatorList.iterator();
//		Iterator<String> keywordIterator = KeywordList.iterator();
//		String current;
//		while(operatorIterator.hasNext()){
//			current=operatorIterator.next();
//			operator_regex+=Pattern.quote(current)+"|";
//
//		}
//		while(keywordIterator.hasNext()){
//			current=keywordIterator.next();
//			keyword_regex+=Pattern.quote(current)+"|";			 
//		}
//		operator_regex=operator_regex.substring(0,operator_regex.length()-1);
//		keyword_regex=keyword_regex.substring(0,keyword_regex.length()-1);
//		System.out.println(keyword_regex);
//		
//		ArrayList<String> keywordList = getKeywordsList();
//
//
//		try {
//
//			tokens=tokenize(data);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//		for(int index=0;index<tokens.size();index++){
//			current=tokens.get(index);
//
//			if(OperatorList.contains(current)){
//				//	System.out.print (" Operator");
//				tokenlist.add(new Token("operator",current));
//				tokens.remove(current);
//			}
//			else if(keywordList.contains(current)){
//				//	System.out.print (" Keyword");
//				tokenlist.add(new Token("keyword",current));
//				tokens.remove(current);}
//			else {
//
//				System.out.println("\n|"+current+"|"+" undetected");
//				regexTokenize(current);
//			}
//		}
//		System.out.println("Remaining"+tokens.size());
//	}
//
//
//	@SuppressWarnings("deprecation")
//	public  ArrayList<String> tokenize(String data) throws IOException{
//
//
//
//
//
//		StringTokenizer st = new StringTokenizer(data);
//		String token;
//		while(st.hasMoreTokens()){
//			token=st.nextToken();
//
//			if(token.length()!=0)tokens.add(token);
//		}
//		return tokens;
//
//	}
//	
//	public static ArrayList<String> getOperatorList(){
//		String data=FileProcess.readFile("operators.csv");
//
//
//		ArrayList<String> operators = new ArrayList<>(); 
//		StringBuffer operator= new StringBuffer();
//		char[] dataArray = data.toCharArray();
//		for(int i=0;i<data.length();i++){
//			if(dataArray[i]!=','){
//				operator.append(dataArray[i]);
//			}
//			else{
//				operators.add(operator.toString());
//				//	System.out.println("|"+operator.toString()+"|");
//				operator.delete(0, operator.length());
//
//			}
//		}
//		operators.add(",");	
//		return operators;
//
//	}
//	public static ArrayList<String> getKeywordsList(){
//		String data=FileProcess.readFile("keywords.csv");
//
//
//		ArrayList<String> operators = new ArrayList<>(); 
//		StringBuffer operator= new StringBuffer();
//		char[] dataArray = data.toCharArray();
//		for(int i=0;i<data.length();i++){
//			if(dataArray[i]!=','){
//				operator.append(dataArray[i]);
//			}
//			else{
//				operators.add(operator.toString());
//				//	System.out.println("|"+operator.toString()+"|");
//				operator.delete(0, operator.length());
//
//			}
//		}
//
//		return operators;
//
//	}
//	public void  regexTokenize(String token){
//          String remains=   detectKeyword(token);
//          tokens.remove(token);
//          System.out.println("reamaining|"+remains+"|");
//          if(remains.length()!=0)tokens.add(remains);
//		}
//
//
//	
//	public String getStringConstants(String data){
//
//		String code="";
//		int start=0,end=0;
//		Token current;
//		while(data.indexOf("\"")>0){
//			start=0;
//			end=0;
//			start=data.indexOf("\"");
//
//			end=data.indexOf("\"", start+1)+1;
//			
//			current=new Token();
//			current.setType("Contstant");
//			current.setValue(data.substring(start, end));
//
//			tokenlist.add(current);
//			//System.out.println("|||"+data.substring(start, end)+"|||");
//			code= data.substring(0, start)+data.substring(end, data.length());
//
//			data=code;
//		}
//
//
//		return code;
//	}
//    public String detectKeyword(String token){
//
//		Pattern r1 = Pattern.compile(keyword_regex);
//		String remanant1 = token;
//		Matcher m1 = r1.matcher(token);
//		
//		int match_size=0;
//		int matches=0;
//		Token current1=new Token();
//		String match = null;
//		String best_match=null;
//		while (m1.find())
//		{
//			match=m1.group();
//			current1.setType("operator");
//			System.out.println("matching with|"+match+"|");
//		if(match.length()>=match_size){best_match=match;current1.setValue(match);match_size=match.length();
//		
//		}
//	}	if(match_size>0){
//			tokenlist.add(current1);
//			System.out.println("best matched|"+best_match+"|");
//			remanant1=token.replace(best_match, "");
//			
//			tokens.remove(token);
//			if(!remanant1.equals("")) tokens.add(remanant1);}
//          return remanant1;
//    }
//	
//    public String detectOperator(String token){
//		String remanant = "";
//		Pattern r = Pattern.compile(operator_regex);
//
//		Matcher m = r.matcher(data);
//		while (m.find( )) {
//			System.out.println("Found value: detected " + m.group(0)+" detected" );
//			int match_size=0;
//			Token current1=new Token();
//			current1.setType("operator");
//		for(int i=0;i<m.groupCount();i++)	{System.out.println("matched with "+m.group(i)+" ");if(m.group(i).length()>=match_size){current1.setValue(m.group(i));match_size=m.group(i).length();}}
//			tokenlist.add(current1);
//			 remanant=data.replace(m.group(0), "");
//			 System.out.println("Remaining-->"+remanant+"<--Remaining");
//			token=remanant;
//		}
//		return token;
//	}
//	
//	public static void main(String[]args){
//		new Analysis(FileProcess.readFile("test.c"));
//		
//		//getSingleOperatorList();
//	}
//}
