package LexicalAnalyzer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.StringReader;

public class FileProcess {
public static String readFile(String location){
	FileInputStream in = null;
    
StringBuffer out=new StringBuffer();
    try {
       in = new FileInputStream(location);
       
       
       int c;
       while ((c = in.read()) != -1) {
       out.append((char)c);
       }
    }
       catch (Exception e){
    	   e.printStackTrace();
       }
    finally {
       if (in != null) {
          try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
       }
       
    }
	return out.toString();
	
}
@SuppressWarnings("deprecation")
public static String writeFile(String text,String location){
	
    FileOutputStream out = null;
    StringReader data=null;
    try {
    	 data=new StringReader(text);
      
       out = new FileOutputStream(location);
       
       int c;
       while ((c = data.read()) != -1) {
          out.write(c);
       }
    }
       catch (Exception e){
    	   e.printStackTrace();
       }
    finally {
       if (data != null) {
          try {
			data.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
       }
       if (out != null) {
          try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
       }
    }
	return location;
	
}
public static void main(String[]args){
	writeFile(readFile("test.c"),"out.c");
}
}
