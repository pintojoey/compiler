import java.lang.Character.Subset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;


class Table{
	Production value;
	String terminal,non_terminal;
}

public class LL1Temp {


	static ArrayList<Production> Grammar;
	static ArrayList<Table> table;
	public static void main(String[] args) {
		Grammar=getGrammar("Grammar.txt");
		Scanner in=new Scanner(System.in);
		System.out.println("Enter String to Parse:");

		table=getTable();

		String input=in.next();
		//input="(i+i)*i$"
		Parse(input);

	}
    public static void printTable(ArrayList<Table> table){
    	Iterator<Table> iterator = table.iterator();
    	while(iterator.hasNext()){
    		Table current = iterator.next();
    		System.out.println(current.non_terminal+":"+current.terminal+"::"+current.value.LHS+"->"+current.value.RHS);
    	}
    	
    }
    public static ArrayList<Table> getTable() {
		ArrayList<Table> table = new ArrayList<Table>();
		Grammar=getGrammar("Grammar.txt");
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){

			Production current = iterator.next();
			ArrayList<String> first =getFirst(current.RHS);
			Iterator<String> iterator_first = first.iterator();
			while(iterator_first.hasNext()){
				String current_first = iterator_first.next();

				if(current_first.equals("^")){
					ArrayList<String> follow =getFollow(current.LHS.charAt(0));
					Iterator<String> follow_iterator = follow.iterator();
					while(follow_iterator.hasNext()){
						String current_follow = follow_iterator.next();
						//System.out.print(" "+current_follow+" ");
						Table obj=new Table();
						obj.value=current;
						obj.terminal=current_follow;
						//System.out.println(obj.terminal );
						obj.non_terminal=current.LHS;
						//System.out.println(obj.non_terminal+" "+obj.terminal+" "+obj.value.LHS+"->"+obj.value.RHS);
						table.add(obj);
					}
				}

				if(isTerminal(current_first.charAt(0))){
					Table obj=new Table();
					obj.value=current;
					obj.terminal=current_first;
					obj.non_terminal=current.LHS;
					// System.out.println("hello"+obj.non_terminal+" "+obj.terminal+" "+obj.value.LHS+"->"+obj.value.RHS);
					table.add(obj);

				}

			}
		}
		/*Iterator<Table> table_iterator=table.iterator();
		while(table_iterator.hasNext()){
			Table current = table_iterator.next();
			System.out.println(current.non_terminal+" "+current.terminal+" "+current.value.LHS+"->"+current.value.RHS);

		}
		 */
		return table;

	}

	public static ArrayList<Production> getGrammar(String filename){
		ArrayList<Production> Grammar=new ArrayList<Production>();
		String grammar=	File.readFile(filename);

		String[] rule = grammar.split(",");
		ArrayList<String> rules=new ArrayList<String>();
		for(String current:rule){
			rules.add(current);
		}
		Iterator<String> rule_iterator=rules.iterator();
		while(rule_iterator.hasNext()){
			String grammarset=rule_iterator.next();
			String LHS=grammarset.substring(0, grammarset.indexOf("-"));
			Production production;
			StringTokenizer RHSset=new StringTokenizer(grammarset.substring(grammarset.indexOf(">")+1,grammarset.length()),"|");

			while(RHSset.hasMoreTokens()){
				String RHS = RHSset.nextToken();
				production=new Production();
				production.LHS=LHS;
				production.RHS=RHS;
				Grammar.add(production);
			}
		}
		return Grammar;
	}
	public static void printProductions(){
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current = iterator.next();
			System.out.println(current.LHS+"->"+current.RHS);
		}
	}
	public static ArrayList<String> getFirst(String word){
		ArrayList<String> first=new ArrayList<String>();

		if(word.length()==0||word.equals("^")){ 
			if(!first.contains("^"))
			{
				first.add("^");
				//System.out.println("reached1");
			}
		}


		else{
			if(isTerminal(word.charAt(0))){
				//System.out.println("reached2");
				if(!first.contains(word.substring(0,1))){
					first.add(word.substring(0,1));

				}
			}


			else 
			{      
				if(!isTerminal(word.charAt(0))){
					//System.out.println("reached3");

					ArrayList<String> first_non_terminal=getFirstOfNonTerminal(word);
					Iterator<String> first_non_terminal_iterator = first_non_terminal.iterator();
					while(first_non_terminal_iterator.hasNext()){
						String current=first_non_terminal_iterator.next();
						if(!first.contains(current))first.add(current);  //add all except null
					}
				}

			}
		}
		return first;
	}
	public static ArrayList<String> getFirstOfNonTerminal(String word) {
		//System.out.println("reached4");
		ArrayList<String> first_non_terminal=new ArrayList<String>();

		if(hasNull(word.substring(0,1))){
			if(word.length()==1){
				//System.out.println("reached5");
				first_non_terminal.add("^");
				//System.out.println("first of non t"+first_non_terminal);
			}
			else{
				first_non_terminal.addAll(getFirst(word.substring(1,word.length())));
			}
		}
		ArrayList<String> rhs_list=getRHS(word.substring(0,1));
		Iterator<String>rhs_iterator=rhs_list.iterator();
		while(rhs_iterator.hasNext()){

			String rhs=rhs_iterator.next();
			//System.out.println(rhs);
			if (rhs.charAt(0)=='^'){}
			if(isTerminal(rhs.charAt(0))){
				//System.out.println("reached6");
				first_non_terminal.add(rhs.substring(0, 1));
				//System.out.println("first of non t"+first_non_terminal);
			}
			if(!(isTerminal(rhs.charAt(0)) || rhs.charAt(0)=='^')){
				//System.out.println("reached7");
				if(!word.substring(0, 1).equals(rhs.substring(0,1))){
					//System.out.println("reached8");
					first_non_terminal.addAll(getFirstOfNonTerminal(rhs));
					//System.out.println("first of non t123"+first_non_terminal);
				}

				//System.out.println("first of non t"+first_non_terminal);
			}
		}
		//System.out.println("first of non t"+first_non_terminal);
		return first_non_terminal;
	}

	public static ArrayList<String> getFollow(char letter){
		ArrayList<String> follow=new ArrayList<String>();
		ArrayList<Production> followRhs=getFollowRhs(letter);
		if(Grammar.get(0).LHS.equals(String.valueOf(letter))){

			follow.add("$");
		}

		Iterator<Production> iterator = followRhs.iterator();
		while(iterator.hasNext()){
			Production current = iterator.next();
			//System.out.println(current.LHS+"->"+current.RHS);
			if(current.RHS.charAt(current.RHS.length()-1)==letter){
				if(!(current.LHS.charAt(0)==current.RHS.charAt((current.RHS.length()-1)))){

					ArrayList<String> temp_follow = getFollow(current.LHS.charAt(0));
					//System.out.println("hellofollow of "+current.LHS.charAt(0)+""+temp_follow);
					Iterator<String> iterator_temp_follow = temp_follow.iterator();
					while(iterator_temp_follow.hasNext()){
						String current1 = iterator_temp_follow.next();
						if(!follow.contains(current1))
							follow.add(current1);
					}
					//follow.addAll(getFollow(current.LHS.charAt(0)));
				}
			}
			else{
				char follow_char=current.RHS.charAt(current.RHS.indexOf(letter)+1);
				if(isTerminal(follow_char)){
					if(!follow.contains(String.valueOf(follow_char)))follow.add(String.valueOf(follow_char));

				}
				else{
					ArrayList<String> first = getFirst(String.valueOf(follow_char));
					//System.out.println("first of "+follow_char+""+first);
					Iterator<String> iterator_first = first.iterator();
					while(iterator_first.hasNext()){
						String current1 = iterator_first.next();
						if(!(current1.equals("^"))){
							if(!follow.contains(current1))
								follow.add(current1);
						}

						if(first.contains("^")){
							if(!(current.LHS.charAt(0)==current.RHS.charAt(current.RHS.indexOf(letter)))){
								ArrayList<String> temp_follow = getFollow(current.LHS.charAt(0));
								Iterator<String> iterator_temp_follow = temp_follow.iterator();
								while(iterator_temp_follow.hasNext()){
									current1 = iterator_temp_follow.next();
									if(!follow.contains(current1))
										follow.add(current1);
								}
								//follow.addAll();


							}
						}
					}
				}


			}


		}
		return follow;
	}		
	public static ArrayList<Production> getFollowRhs(char letter){
		Iterator<Production> iterator = Grammar.iterator();
		ArrayList<Production> followRhs=new ArrayList<Production>();
		while(iterator.hasNext()){
			Production current = iterator.next();
			for(int i=0;i<current.RHS.length();i++){
				if(current.RHS.charAt(i)==letter){
					followRhs.add(current);  
					break;
				}
			}
		}
		return followRhs;

	}
	static boolean isTerminal(char x){
		if (x=='^') return false;
		else if((int)x>=65 && (int)x<=91)return false;
		else return true;
	}
	static boolean hasNull(String LHS){
		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current=iterator.next();
			if(LHS.equals(current.LHS)){
				if(current.RHS.equals("^"))
					return true;
			}
		}
		return false;
	}
	static ArrayList<String> getRHS(String LHS){
		ArrayList<String> list=new ArrayList<String>();

		Iterator<Production> iterator = Grammar.iterator();
		while(iterator.hasNext()){
			Production current=iterator.next();
			if(LHS.equals(current.LHS)){
				list.add(current.RHS);
			}
		}
		return list;
	}
	static void Parse(String input){
		String stack="$E";
		System.out.println("Stack        Input     Action");
		while(true){
			System.out.print(stack+"         "+input+"        ");
			if(!isTerminal(getTopChar(stack))){
				Production production = getTableProduction(getTopString(stack), getStartString(input));
				stack=stack.substring(0,stack.length()-1);
				if(!production.RHS.equals("^")){

					StringBuilder b=new StringBuilder(production.RHS);
					b.reverse();
					stack=stack+b.toString();
				}
				System.out.println(production.LHS+"->"+production.RHS);
			}
			else{

				if(getTopString(stack).equals("$")&& getStartString(input).equals("$")){
					System.out.println("Accepted");
					break;
				}
				if(!getTopString(stack).equals(getStartString(input)))
					System.exit(-1);
				stack=stack.substring(0,stack.length()-1);
				input=input.substring(1,input.length());
				System.out.println("Match");

			}
		}

	}
	static String getStartString(String word){
		return word.substring(0, 1);
	}
	static String getTopString(String word){
		return word.substring(word.length()-1, word.length());
	}
	static char getTopChar(String word){
		return word.charAt(word.length()-1);
	}
	static Production getTableProduction(String non_terminal, String terminal){
		Iterator<Table> iterator = table.iterator();
		while(iterator.hasNext()){
			Table current=iterator.next();
			if(current.terminal.equals(terminal)&&current.non_terminal.equals(non_terminal)){
				return current.value;
			}
		}
		return null;
	}
}